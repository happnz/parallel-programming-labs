package com.company.thread;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileClassParser implements Callable<Map<String,Set<String>>> {
    private static Pattern pattern = Pattern.compile("[\\w]+\\s(extends|implements).+\\{+");

    private final File file;

    public FileClassParser(File file) {
        this.file = file;
    }

    private Map<String, String> createParentToChildMap(String s) {
        String[] split = s.split("(\\sextends\\s|\\simplements\\s|\\s\\{|,\\s)");
        String child = split[0];
        Map<String, String> parentToChildMap = new HashMap<>();
        for (int i = 1; i < split.length; i++) {
            String parent = split[i];
            parentToChildMap.put(parent, child);
        }

        return parentToChildMap;
    }

    @Override
    public Map<String, Set<String>> call() {
        Map<String, Set<String>> fileMap = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            reader.lines().forEach(line -> {
                Matcher matcher = pattern.matcher(line);
                if (matcher.find()) {
                    String s = matcher.group();
                    Map<String, String> lineMap = createParentToChildMap(s);
                    lineMap.forEach((key, value) -> {
                        Set<String> children = fileMap.getOrDefault(key, new HashSet<>());
                        children.add(lineMap.get(key));
                        fileMap.put(key, children);
                    });
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileMap;
    }
}
