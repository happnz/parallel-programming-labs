package com.company;

import com.company.thread.FileClassParser;

import java.io.File;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Program {
    private static final String SRC_FOLDER = "./src";
    private static final Lock LOCK = new ReentrantLock();

    private Map<String, Set<String>> classMap = new HashMap<>();
    private List<Future<Map<String, Set<String>>>> futureList = new ArrayList<>();

    public Map<String, Set<String>> getClassMap() {
        return classMap;
    }

    public void runProgram() {
        File src = new File(SRC_FOLDER);

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        listFilesForFolder(src, executorService);
        executorService.shutdown();

        futureList.forEach(future -> {
            try {
                future.get().forEach((key, value) -> {
                    LOCK.lock();
                    try {
                        Set<String> children = classMap.getOrDefault(key, new HashSet<>());
                        children.addAll(value);
                        classMap.put(key, children);
                    } finally {
                        LOCK.unlock();
                    }
                });
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
    }

    public void printClassMap() {
        classMap.forEach((key, value) -> System.out.println(key + " : " + value));
    }

    private void listFilesForFolder(final File folder, final ExecutorService executorService) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry, executorService);
            } else {
                FileClassParser fileClassParser = new FileClassParser(fileEntry);
                Future<Map<String, Set<String>>> future = executorService.submit(fileClassParser);
                futureList.add(future);
            }
        }
    }
}
