package com.company;

import org.junit.Ignore;
import org.junit.Test;

import java.util.Map;
import java.util.Set;

public class RaceConditionTest {

    @Test
    @Ignore
    public void testRaceCondition() {
        Program program = new Program();
        program.runProgram();
        Map<String, Set<String>> classMap = program.getClassMap();
        for (int i = 0; i < 1000; i++) {
            Program program2 = new Program();
            program2.runProgram();
            Map<String, Set<String>> newClassMap = program2.getClassMap();
            if (!classMap.equals(newClassMap)) {
                System.out.println("Iteration #" + (i+1));
                System.out.println("Initial class map:");
                program.printClassMap();
                System.out.println("\nNew class map:");
                program2.printClassMap();
                System.exit(-1);
            }
        }
    }
}
